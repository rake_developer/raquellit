import { LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

    
    static get properties(){
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }
    constructor(){
        super();

        this.people = [
            {
                name:"Pepe",
                year: 1,
                photo: {
                    src: "https://randomuser.me/api/portraits/men/20.jpg",
                    alt: "Foto de Pepe"
                },
                profile: "texto de prueba 1"
            }, {
                name: "Juan",
                year: 3,
                photo: {
                    src: "https://randomuser.me/api/portraits/men/1.jpg",
                    alt: "Foto de Juan"
                },
                profile: "texto de prueba 2"
            },{
                name: "Raquel",
                year: 14,
                photo: {
                    src: "https://randomuser.me/api/portraits/women/6.jpg",
                    alt: "Foto de Raquel"
                },
                profile: "texto de prueba 3. sfh sdkfhlskdfh ksf slkdfh s lsksi e sldkdshfshf sifhsldfkjsfh"
            },{
                name: "Maria",
                year: 1,
                photo: {
                    src: "https://randomuser.me/api/portraits/women/8.jpg",
                    alt: "Foto de María"
                },
                profile: "texto de prueba 4"
            },{
                name: "Ana",
                year: 6,
                photo: {
                    src: "https://randomuser.me/api/portraits/women/10.jpg",
                    alt: "Foto de Ana"
                },
                profile: "texto de prueba 5. sdofisfhsf  sdfsdfk ksdjf sfh ksf skfh kjf skfshkshff"
            }
        ];
        this.showPersonForm=false;
    }
    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if(this.showPersonForm===true){
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }
        if(changedProperties.has("people")){
            console.log("Ha cambiado la propiedad people en PersonaMain");
            this.dispatchEvent(
                new CustomEvent(
                    "update-people",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            );
        }
    }
    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando listado de personas");

        this.shadowRoot.getElementById("peoplelist").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
       
    }
    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando formulario de personas");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peoplelist").classList.add("d-none");
        
    }
    render() {
        /*Si el tipo de datos es complejo, tipo los objets es necesario pasar
        el valor como atributo. Eso se hace poniendo el . delante*/
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">Empleados</h2>
            <div class="row" id="peoplelist">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html` <persona-ficha-listado 
                                            @delete-person="${this.deletePerson}"
                                            @info-person="${this.infoPerson}"
                                            fname="${person.name}" 
                                            yearInCompany="${person.year}"
                                            profile="${person.profile}"
                                            .photo="${person.photo}">
                                        </persona-ficha-listado>`
                    )}
                </div>
                
            </div>
            <div class="row">
                 <persona-form 
                 @save-person="${this.savePerson}" 
                 @person-form-close="${this.personFormClose}" 
                 id="personForm" 
                 class="d-none border rounded border-primary">
                </persona-form>
            </div>
            
        
        `;

    }
    deletePerson(e){
        console.log("deletePerson");
        console.log("Se va a borrar la persona="+e.detail.name);
        this.people=this.people.filter(
            person => person.name != e.detail.name
        );
    }
    personFormClose(e){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario del nuevo empleado");

        this.showPersonForm = false;  
    }
    savePerson(e){
        console.log("savePerson");
        console.log(e.detail.person);

        if(e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona: "+e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                ? person = e.detail.person : person
            );

        }else{
            console.log("Se va a almacenar la persona: "+e.detail.person.name);
            //this.people.push(e.detail.person);
            this.people = [...this.people, e.detail.person];
            console.log("Persona almacenada");
        }

        console.log("Fin proceso"); 

        this.showPersonForm = false;
        
    
    }
    infoPerson(e){
        console.log("infoPerson");
        console.log("Se ha pedido información de la persona="+e.detail.name);

        let choosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        //console.log(choosenPerson);
        
        this.shadowRoot.getElementById("personForm").person = choosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

}

 

customElements.define('persona-main', PersonaMain)