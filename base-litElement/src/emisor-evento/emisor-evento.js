import { LitElement, html} from 'lit-element';

class EmisorEvento extends LitElement {

    render() {

        return html`

            <h3>emisor evento</h3>
            <button @click="${this.sendEvent}">No pulsar</button>
        `;

    }
    sendEvent(e){
        console.log("sendEvent");
        console.log("pulsado boton");

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    detail:{
                        course:"TechU",
                        year:2020
                    }
                }
            )
        )
    }

}

 

customElements.define('emisor-evento', EmisorEvento)