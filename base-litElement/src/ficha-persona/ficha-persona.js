import { LitElement, html} from 'lit-element';

class FichaPersona extends LitElement {

    static get properties(){
        
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}

        };
    }

    constructor() {
        super();
        this.name = "Fulanito de tal";
        this.yearsInCompany = 33;

       this.updatePersonInfo();

        this.photo = {
            src: "https://randomuser.me/api/portraits/men/20.jpg",
            alt: "Foto del personaje"
        }
    }

    updated(changedProperties){
        changedProperties.forEach((oldvalue,propName) => {
            console.log("Propiedad: "+propName+" cambia valor, anterior era:"+oldvalue);
        });

        if(changedProperties.has("name")){
            console.log("El nombre ha cambiado. El valor anterior es: "+ changedProperties.get("name") +
                " el nuevo es: "+ this.name);
        }
        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany cambiada valor anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }

    render() {

        return html`
    
            <div>
                <label for="fname"> Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label for="yearsInCompany"> Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYears}"></input>
                <br />
                <input type="text" name="personInfo" value="${this.personInfo}" disabled></input>
                <br />

                <img src="${this.photo.src}" heigth="200" width="200" alt="${this.photo.alt}">
            </div>

        `;

    }

    
    updateName(e){
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYears(e){
        console.log("updateYear");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo(){
        
        if(this.yearsInCompany >= 7){
            this.personInfo = "lead";
        }else if(this.yearsInCompany >= 5){
            this.personInfo = "senior";
        }else if(this.yearsInCompany >= 3){
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
        
    }

}

 

customElements.define('ficha-persona', FichaPersona)