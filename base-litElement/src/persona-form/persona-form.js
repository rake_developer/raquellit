import { LitElement, html} from 'lit-element';

class PersonaForm extends LitElement {

    static get properties(){
        return{
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }
    constructor(){
        super();
        this.resetFormData();
    }
    render() {

        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}" id="personFormName" class="form-control" placeholder="Nombre Completo"/>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" @input="${this.updateYear}" .value="${this.person.year}" class="form-control" placeholder="Años en la empresa"/>
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.savePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>

        `;

    }
    goBack(e){
        console.log("goBack");
        //para evitar que el formulario se envíe
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("person-form-close",{}));
    }

    savePerson(e){
        console.log("savePerson");
        e.preventDefault();

        console.log("Name = "+this.person.name);
        console.log("Year = "+this.person.year);
        console.log("Profile = "+this.person.profile);
        
        if (this.editingPerson === false) {
            this.person.photo = {
                "src": "https://randomuser.me/api/portraits/women/"+(Math.floor(Math.random() * 10) + 1)+".jpg",
                "alt": "Foto emplead@"
            }
        }
        

        this.dispatchEvent(
            new CustomEvent(
                "save-person",
                {
                    detail:{
                        person: {
                            name: this.person.name,
                            year: this.person.year,
                            profile: this.person.profile,
                            photo: this.person.photo
                        },
                        editingPerson: this.editingPerson
                    }
                }
            )
        );
        this.resetFormData();

    }
    updateName(e){
        console.log("updateName");
        console.log("se actualiza el nombre:" + e.target.value);
        this.person.name = e.target.value;
    }
    updateProfile(e){
        console.log("updatePerfil");
        console.log("se actualiza el perfil:" + e.target.value);
        this.person.profile = e.target.value;
    }
    updateYear(e){
        console.log("updateYear");
        console.log("se actualiza el año:" + e.target.value);
        this.person.year = e.target.value;
    }
    resetFormData(){
        console.log("resetFormData");
        this.person = {};
        this.person.name ="";
        this.person.profile ="";
        this.person.year ="";

        this.editingPerson=false;
    }
    
}

 

customElements.define('persona-form', PersonaForm)